package ru.t1.panasyuk.tm.api.repository;

import ru.t1.panasyuk.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    boolean existsById(String id);

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    int getSize();

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}