package ru.t1.panasyuk.tm.api.controller;

public interface IProjectController {

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void createProject();

    void clearProjects();

    void completeProjectById();

    void completeProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjects();

    void showProjectById();

    void showProjectByIndex();

    void startProjectById();

    void startProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}