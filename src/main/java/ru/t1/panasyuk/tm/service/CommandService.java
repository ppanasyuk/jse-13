package ru.t1.panasyuk.tm.service;

import ru.t1.panasyuk.tm.api.repository.ICommandRepository;
import ru.t1.panasyuk.tm.api.service.ICommandService;
import ru.t1.panasyuk.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}